package com.example.terstruktur;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
private EditText celcius, kelvin, fahrenheit, reamur;
private Button konversi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Konversi Suhu");

        celcius = findViewById(R.id.txtcelcius);
        fahrenheit = findViewById(R.id.txtFahrenheit);
        reamur = findViewById(R.id.txtreamur);
        kelvin = findViewById(R.id.txtkelvin);
        konversi = findViewById(R.id.btnKon);

        konversi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String rc = celcius.getText().toString();

                double c = Double.parseDouble(rc);
                int c1 = Integer.parseInt((rc));

                int fah = (c1*9/5)+32;
                double ream = c*4/5;
                double kel = c+273.15;

                fahrenheit.setText("" + fah);
                reamur.setText("" + ream);
                kelvin.setText("" + kel);
            }
        });
    }
}
