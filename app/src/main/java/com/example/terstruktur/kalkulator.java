package com.example.terstruktur;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class kalkulator extends AppCompatActivity {
    private Button ac,satu,dua,tiga,empat,lima,enam,tujuh,delapan,sembilan,kali,bagi,tambah,kurang,koma,samdeng,nol;
    private TextView hasil;
    private final char kalii = '*';
    private final char bagii = '/';
    private final char tambahh = '+';
    private final char kurangg = '-';
    private final char samdengg = 0;
    private double val1 = Double.NaN;
    private double val2;
    private char aksi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kalkulator);
        getSupportActionBar().setTitle("Calculator");

        ac = findViewById(R.id.ac);
        satu = findViewById(R.id.satu);
        dua = findViewById(R.id.dua);
        tiga = findViewById(R.id.tiga);
        empat = findViewById(R.id.empat);
        lima = findViewById(R.id.lima);
        enam = findViewById(R.id.enam);
        tujuh = findViewById(R.id.tujuh);
        delapan = findViewById(R.id.delapan);
        sembilan = findViewById(R.id.sembilan);
        nol = findViewById(R.id.nol);
        kali = findViewById(R.id.kali);
        bagi = findViewById(R.id.bagi);
        tambah = findViewById(R.id.tambah);
        kurang = findViewById(R.id.kurang);
        samdeng = findViewById(R.id.samdeng);
        koma = findViewById(R.id.koma);
        hasil = findViewById(R.id.hasil);




        satu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "1");
            }
        });
        dua.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "2");
            }
        });
        tiga.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "3");
            }
        });
        empat.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "4");
            }
        });
        lima.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "5");
            }
        });
        enam.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "6");
            }
        });
        tujuh.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "7");
            }
        });
        delapan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "8");
            }
        });
        sembilan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "9");
            }
        });
        nol.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                hasil.setText(hasil.getText().toString() + "0");
            }
        });
        tambah.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                compute();
                aksi = tambahh;
                hasil.setText(String.valueOf(val1) + "+");

            }
        });
        kali.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                compute();
                aksi = kalii;
                hasil.setText(String.valueOf(val1) + "*");

            }
        });
        bagi.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                compute();
                aksi = bagii;
                hasil.setText(String.valueOf(val1) + "/");

            }
        });
        kurang.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                compute();
                aksi = kurangg;
                hasil.setText(String.valueOf(val1) + "-");

            }
        });
        samdeng.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                compute();
                aksi = samdengg;
                hasil.setText(hasil.getText().toString() + String.valueOf(val2) + "=" + String.valueOf(val1));

            }
        });
        ac.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (hasil.getText().length() > 0){
                    CharSequence name = hasil.getText().toString();
                    hasil.setText(name.subSequence(0, name.length()-1));

                }
                else{
                    val1 = Double.NaN;
                    val2 = Double.NaN;
                    hasil.setText(null);
                }
            }
        });



    }
    private void compute(){
        if (!Double.isNaN(val1)){
            val2 = Double.parseDouble((hasil.getText().toString()));

            switch (aksi){
                case kalii:
                    val1 = val1 * val2;
                    break;
                case bagii:
                    val1 = val1 / val2;
                    break;
                case tambahh:
                    val1 = val1 + val2;
                    break;
                case kurangg:
                    val1 = val1 - val2;
                    break;
                case samdengg:
                    break;
            }
        }
        else {
            val1 = Double.parseDouble(hasil.getText().toString());
        }
    }
}
